package 
{
	import adobe.utils.CustomActions;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	/**
	 * LifeGame.as
	 * @author world
	 */
[SWF(width = "780", height = "600", backgroundColor = "0x001100", frameRate = "24")]
	public class LifeGame extends Sprite 
	{
		private var map:Array;
		private var nextMap:Array;
		private const MAX_ROWS:int = 17;
		private const MAX_COLS:int = 24;
		private var runSpeed:int;
		private var createP:Number;
		private var ns_timer:Timer;
		
		private var ss_b:StartStopButton;
		private var r_b:SimpleButton;
		private var c_b:SimpleButton;
		private var cl_b:SimpleButton;
	
		public function LifeGame():void 
		{
			stage.scaleMode = "noScale";
			//初期画面の作成
			init();
		}
		private function init():void 
		{
			//1msに何回判定を行うかを設定 (1000 / runSpeed [回] / 1[ms])
			runSpeed = 300;
			
			//リセット時生物が出現する確立 ( (1.00-createP) / 100 [%] )
			createP = 0.80;
			
			//背景色変更
			change_backColor(0xBFFFBF);
			
			//ゲーム画面設置
			init_map();
			
			var work_x:int = 0;
			//スタートストップボタン設置
			ss_b = new StartStopButton();
			ss_b.x = work_x;
			addChild(ss_b);
			ss_b.addEventListener(MouseEvent.CLICK, onStartStop_bClick);	
			
			//リセットボタン設置
			r_b = new SimpleButton(Images.ImageReset_b);
			r_b.x = work_x += ss_b.width;
			addChild(r_b);
			r_b.addEventListener(MouseEvent.CLICK, onReset_bClick);
			
			//クリアボタン設置
			cl_b = new SimpleButton(Images.ImageClear_b);
			cl_b.x = work_x += r_b.width;
			addChild(cl_b);
			cl_b.addEventListener(MouseEvent.CLICK, onClear_bClick);
			
			//タイマーの設定
			ns_timer = new Timer(runSpeed);
			ns_timer.addEventListener(TimerEvent.TIMER, onTick);
		}
		private function init_map():void 
		{
			//Creatureを並べる
			map = new Array();
			for (var i:int = 0; i < (MAX_ROWS * MAX_COLS); i++)
			{
				var x:int = i % MAX_COLS;
				var y:int = i / MAX_COLS;
				var p:Creature = new Creature();
				map[i] = p;
				p.x = x * p.width;
				p.y = y * p.height + 32;
				if (Math.random() > createP) p.setLive();
				addChild(p);
			}
		}
		private function onStartStop_bClick(e:Event):void 
		{
			(ss_b.isStart) ? ns_timer.stop() : ns_timer.start();
		}
		private function onReset_bClick(e:Event):void 
		{
			if (ss_b.isStart)
			{
				init_map();
			}
		}
		private function onClear_bClick(e:Event):void 
		{
			var tmp_createP:Number = createP;
			createP = 1;
			init_map();
			createP = tmp_createP;
		}
		private function onTick(e:Event):void 
		{
			var isLiveMap:Array = new Array();
			
			//世代交代
			for (var i:int = 0; i < (MAX_ROWS * MAX_COLS); i++)
			{
					var x:int = i % MAX_COLS;
					var y:int = i / MAX_COLS;
					
					isLiveMap[i] = nextTurn(x, y);	
			}
			for (var j:int = 0; j < isLiveMap.length; j++)
			{
				var tmp_cr:Creature=map[j];
				(isLiveMap[j]) ? tmp_cr.setLive() : tmp_cr.setDead();
			}
		}
		private function nextTurn(x:int, y:int):Boolean
		{
			var countLiveCreature:int = 0;
			var k:int = y * MAX_COLS + x;
			var medCreature:Creature = map[k];	
			//周囲8マスの生体数を数え上げる
			for (var i:int = -1; i <= 1; i++)
			{
				for (var j:int = -1; j <= 1; j++)	
				{	
					if ((x + i >= 0) && (x + i < MAX_COLS)
					 && (y + j >= 0) && (y + j < MAX_ROWS)
					 && ((i != 0) || (j != 0)))
					{
						var cr:Creature = new Creature();
						cr = map[k + i + MAX_COLS * j];
						if (cr.isLive)
						{
							countLiveCreature ++;
						}
					}
				}
			}
			if(countLiveCreature!=0)
				trace(countLiveCreature);
			//世代交代
			if (medCreature.isLive)
			{
				if ((countLiveCreature == 2)
				  ||(countLiveCreature == 3)) { return true; }
				else
				{
					return false;
				}
			}
			else
			{
				if (countLiveCreature == 3)
				{
					return true;
				}
				else { return false; }
			}
		}
		private function change_backColor(col:int):void 
		{
			//背景色用矩形
			var bg_shape:Shape = new Shape();//背景用オブジェクト
			bg_shape.graphics.beginFill(col);//塗りつぶしカラー(RGB値:0xRRGGBB形式)
			bg_shape.graphics.drawRect(0,0,stage.stageWidth,stage.stageHeight);//矩形(X座標,Y座標,幅,高さ)
			addChildAt(bg_shape,0);//表示リストレイヤー0に矩形を追加
		}
	}
}