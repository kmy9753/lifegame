package  
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author world
	 */
	public class StartStopButton extends Sprite
	{
		//埋め込みいイメージの指定
		[Embed(source = '/../img/start_b.gif')]private static const ImageStart_b:Class;
		[Embed(source = '/../img/stop_b.gif')]private static const ImageStop_b:Class;
		
		private var cur_bmp:Bitmap;
		public var isStart:Boolean;
		
		public function StartStopButton() 
		{
			setStart();
			buttonMode = true;
			addEventListener(MouseEvent.CLICK, onClick);
		}
		
		private function changeImage(bmpClass:Class):void 
		{
			removeBmp();
			cur_bmp = new bmpClass as Bitmap;
			addChild(cur_bmp);
			isStart = (bmpClass == ImageStart_b) ? true : false;
		}
		
		private function removeBmp():void 
		{
			if (cur_bmp != null) removeChild(cur_bmp);
		}
		
		public function setStart():void 
		{
			changeImage(ImageStart_b);
		}
		
		public function setStop():void 
		{
			changeImage(ImageStop_b);
		}
		
		private function onClick(e:Event):void 
		{
			(isStart) ? setStop() : setStart();
		}
				
	}

}