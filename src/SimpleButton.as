package  
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	/**
	 * ResetButton.as
	 * @author world
	 */
	public class SimpleButton extends Sprite
	{	
		private var bmp:Bitmap;
		
		public function SimpleButton(imgClass:Class):void 
		{
			buttonMode = true;
			bmp = new imgClass as Bitmap;
			addChild(bmp);
		}
	}

}
