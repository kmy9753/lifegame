package  
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.*;
	/**
	 * Creature.as
	 * @author world
	 */
	public class Creature extends Sprite
	{	
		public function Creature() 
		{
			setDead();
			addEventListener(MouseEvent.CLICK, onClick);
		}
		private function onClick(e:MouseEvent):void 
		{
			(isLive) ? setDead() : setLive();
		}

		//表示中のBitmapを管理する変数
		private var cur_bmp:Bitmap = null;
		
		private function removeBmp():void 
		{
			if (cur_bmp != null) removeChild(cur_bmp);
		}
		
		//生死のフラグ(true: 生/ false: 死)
		public var isLive:Boolean;
		private function changeImage(bmpClass:Class):void 
		{
			removeBmp();
			cur_bmp = new bmpClass as Bitmap;
			addChild(cur_bmp);
			isLive = (bmpClass == Images.ImageDead) ? false : true;
		}
		
		public function setLive():void 
		{
			changeImage(Images.ImageLive);
		}
		public function setDead():void 
		{
			changeImage(Images.ImageDead);
		}
		
	}

}