package  
{
	/**
	 * Buttons
	 * @author world
	 */
	public class Images
	{
//埋め込みたいイメージの指定
//please add new imgs
		[Embed(source = '/../img/dead.gif')]public static const ImageDead:Class;
		[Embed(source = '/../img/live.gif')]public static const ImageLive:Class;
		[Embed(source = '/../img/reset_b.gif')]	public static const ImageReset_b:Class;	
		[Embed(source = '/../img/start_b.gif')]	public static const ImageStart_b:Class;	
		[Embed(source = '/../img/stop_b.gif')]	public static const ImageStop_b:Class;
		[Embed(source = '/../img/close_b.gif')]	public static const ImageClose_b:Class;	
		[Embed(source = '/../img/clear_b.gif')]	public static const ImageClear_b:Class;	
		[Embed(source = '/../img/numberUp_b.gif')]	public static const ImageNumberUp_b:Class;	
		[Embed(source = '/../img/numberDown_b.gif')]	public static const ImageNumberDown_b:Class;	
		
		public function Images() 
		{
			
		}
		
	}

}